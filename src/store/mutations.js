import { updateField } from 'vuex-map-fields'

export default {
  updateField,
  UPDATE_PAGENUMBER (state, payload) {
    if (payload) {
      state.pageNumber++
    } else {
      state.pageNumber--
    }
  },
  UPDATE_QUERYPARAM (state, payload) {
    state.queryParam = payload
  },
  UPDATE_RECIPES (state, payload) {
    state.recipes = payload
  },
  UPDATE_ERROR (state, payload) {
    state.error = payload
  },
  RESET_PAGENUMBER (state, payload) {
    state.pageNumber = payload
  },
  RESET_STORE (state) {
    state.recipes = []
  }
}
