import axios from 'axios'

export default {
  handlePagination ({ commit, state }, payload) {
    commit('UPDATE_PAGENUMBER', payload)
  },
  buildQueryParam ({ commit, state }) {
    let tempIngredients = ''
    let tempKeyword = ''
    let tempPageNumber = state.pageNumber
    if (state.ingredients.length) {
      tempIngredients = state.ingredients.join(',').replace(' ', '+')
    }
    if (state.keyword.length > 0) {
      tempKeyword = state.keyword.replace(' ', '+')
    }
    let queryParam = { i: tempIngredients, q: tempKeyword, p: tempPageNumber }
    Object.keys(queryParam).forEach((key) => (queryParam[key] === '') && delete queryParam[key])
    commit('UPDATE_QUERYPARAM', queryParam)
  },
  async getRecipes ({ commit, state }) {
    try {
      const response = await axios.get(`/api/`, { params: state.queryParam })
      if (response.data.results.length !== 0) {
        commit('UPDATE_RECIPES', response.data.results)
        commit('UPDATE_ERROR', false)
      } else {
        commit('UPDATE_ERROR', true)
      }
    } catch (error) {
      console.error(error)
    }
  },
  resetPageNumber ({ commit, state }, payload) {
    commit('RESET_PAGENUMBER', payload)
  },
  resetStore ({ commit }) {
    commit('RESET_STORE')
  }
}
