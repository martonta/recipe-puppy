import Vue from 'vue'
import App from './App.vue'
import store from './store/index'
import router from './router/routes'
import Buefy from 'buefy'

Vue.use(Buefy)

Vue.config.productionTip = false

require('./assets/main.scss')

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
