import Vue from 'vue'
import VueRouter from 'vue-router'
import RecipePuppy from '../views/Recipe-Puppy'

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    {
      path: '/',
      component: RecipePuppy
    }
  ]
})

export default router
