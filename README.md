# Vue Recipe Lab

This project is a small experiment with Vue JS, Buefy and the Recipe Puppy API.

Started with Vuex, but considering it as an overkill I switched to parent-children communication using props and emits, but handling pagination and getting queryParams from different components made it  difficult to keep all the states constantly updated and to provide valid information for all the components using the same values. Finally I switched back to Vuex to keep application state in a different place where components can modify it via actions and mutations. My aim was to keep all the necessary "business logic" inside the Vuex store and keep it accessible for all the components.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
