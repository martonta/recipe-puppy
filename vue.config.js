module.exports = {
  devServer: {
    proxy: {
      '^/api': {
        target: 'http://www.recipepuppy.com',
        ws: true,
        changeOrigin: true
      }
    }
  }
}
