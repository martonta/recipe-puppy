import { shallowMount } from '@vue/test-utils'
import RecipePuppy from '@/views/Recipe-Puppy.vue'

describe('RecipePuppy.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(RecipePuppy, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
})
